---
pagetitle: Quentin Astegiano - CV 2017
title: Expert Craftsman Java
css:
  - assets/css/styles.css
  - assets/css/styles-qas.css
author: Quentin Astegiano
email: quentin@astegiano.org
tel: 06 88 45 04 72
address: 
  - 51 rue Carnot
  - 94700 Maisons Alfort
education:
  - degree: Ingénieur en Génie Logiciel
    place: EFREI, Villejuif
    time: 2005
  - degree: Bac Scientifique
    place: Manosque
    time: 2000
language:
  - lang: Français
    level: Langue maternelle
  - lang: Anglais
    level: Ecrit courant, oral bon niveau
website:
  - url: https://bitbucket.org/QuentinAstegiano/ 
    label: Mes projets perso sur BitBucket
    icon: code-fork
  - url: https://www.linkedin.com/in/quentin-astegiano-518137135
    label: LinkedIn
    icon: linkedin
strength:
  - Expert Technique
  - Autonome
  - Adaptable
  - Bon communicant
  - Rigoureux
---

# Mon profil

Après **plus de 10 ans** passés à travailler dans le domaine du **développement Java**, j'ai acquis une expérience importante que ce soit pour la **conception de logiciels**, **l'architecture**, ou **les implémentations**. J'ai mis en place et développé des **applications web performantes et à haute disponibilité**, des **services REST JSON**, tout comme des **clients lourds** et des **back offices**.
J'ai conçu et implémenté des logiciels utilisant **ElasticSearch**, **DropWizard**, **Guice**... 

Je maîtrise les outils nécessaire à la création de solutions de qualité, comme **Git**, **Jenkins**, **IntelliJ**, **Gradle**, ou encore **Jira**.

Grand **adepte de Test Driven Development**, j'ai contribué à introduire cette méthode dans mes équipes, tout comme les **revues de code** et le **pair programming**.

Je mets actuellement mes compétences au service de Darty, chez lequel j'interviens comme **Directeur Technique du domaine Commerce Cross Canal**.

# Compétences

* Java: **Java 5 à 8**, Dropwizard, Jetty, Jackson, Guava, Guice
* Méthodes: **Test Driven Developpement**, Domain Driven Developpement, Agile
* Moteur d'indexation: **ElasticSearch**, Endeca
* Builds: **Gradle**, Maven
* Scripts: **Bash**, **Groovy**, Python
* Outils: **Git**, Jenkins, Jira, Logstash, Kibana, Graphite, Apache Kafka
* IDE: **IntelliJ Idea**, **Vi**, Eclipse 
* OS: **Linux**, Windows 
* Web: Javascript, jQuery, HTML, CSS
* BDD: SQL, DB2

# 2006 - Présent: Darty

## 2015 - Présent: Directeur Technique

En charge de la **Direction Technique du domaine Commerce Cross Canal**, **architecte principal** sur les différents projets traités par les équipes du domaine.

Réalisation des **analyses d'impact** sur la quasi-totalité des projets étudiés.

Réalisation des **documents d'architecture** des sujets principaux, comme la refonte des espaces clients Darty, le développement du site DartyPro, ou la vente de prestations sur les sites Internet.

**Implémentations** des projets les plus critiques, comme la **mise en place d'ElasticSearch** (solution d'indexation des catalogues produits) ou l'amélioration de la sécurité des fronts de vente.

Pilotage d’une **bascule des codes sources dans BitBucket**, et mise en place de nouveaux processus de développement à base de **code review** et de **pair programming**.

Homogénéisation des méthodes de build des différents projets via **un plugin Gradle** spécifique, et mise en place des **pipelines de livraison continue dans Jenkins**.

Mise en place d’une stack **ElasticSearch / Logstash / Kibana** afin d'améliorer le monitoring des applications en production.

## 2013 - 2015: Responsable des applications "Fronts de Vente"

**Gestion d’une équipe de trois à six personnes** qui développent l'intégralité des projets impactant les fronts DartyCom et MisterGoodDeal.

Contribution à la transition d'une architecture monolithique vers une **architecture de services s'appuyant sur DropWizard et Jetty**.

Participation aux **développements de la plupart des projets**, soit directement sur les **implémentations** (projets sur l'intégration d'une place de marché, la refonte ergonomique du site, la création d'un nouveau tunnel d'achat, ...) soit en supervision sur ce qui est réalisé par l'équipe.

Réalisation de plusieurs applications: 

* une **application d'analyse d'historique de ventes** pour aider à la recommandation produit, 
* un service de **comparateur de prix** entre Darty et ses concurrents,
* des applications d'administration éditoriale

Simplification des outils à disposition de l'équipe en basculant les **builds des applications sur Gradle** et en développant des **automatisations des process** via des scripts Bash et Groovy.

## 2011 - 2013: Responsable junior des applications "Fronts de Vente"

Participation à la **création des nouveaux fronts de vente de Darty**.

Développement de l'**intégration du moteur d'indexation Endeca**.

Réalisation des **back offices d'administration**, développés avec le framework Vaadin, à destination des équipes métier.

**Développement de multiples projets**, comme la vente de forfait téléphonique, le développement du merchandising, ou les synergies entre les applications web et magasin.

Contribution à la mise en place d’**un serveur d'intégration continue Hudson**, et création un ensemble de tests d'acceptance sur les fronts.

# 2005 - 2011: Ortec / MasaGroup

## 2009 - 2011: Expert Java

Au sein d'une équipe de cinq personnes:

* Participation au développement du logiciel BKIW.
* Réalisation de la couche front (Java GWT, ExJS) de ce produit de gestion et d'optimisation de planning de collecte de déchets industriels.

## 2006 - 2009: Team Lead Java

Ayant sous ma responsabilité technique une équipe de une à trois personnes :

* Conception et développement de Mapeos Acquisition en Java (Vaadin) et Javascript. Cette application permet aux collectivités locales de saisir, visualiser et modifier des circuits de collecte d'ordures ménagères.

## 2005 - 2006: Développeur Java

Conception et réalisation de l'application Mapeos Information en Java et Javascript pour EcoEmballage. A l'aide du framework de cartographie ESRI, cette solution web permet de présenter graphiquement des données relatives à la collecte de déchets.
