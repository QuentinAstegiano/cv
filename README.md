# CV Templating

The goal of this project is to store my very own CV, and the templates needed to transform it from Markdown to HTML.

## Prerequisite

* Having pandoc installed: http://pandoc.org/
* Create your own CV
* To generate the HTML file, use the following syntax:

```
#!bash
pandoc QuentinAstegiano.md -s --template templates/web/template.html -o templates/web/cv.html
```

## CV Structure

### Header

The header section is used to fill the column on the right.

### Main content

Just write your CV in markdown :)

## Todo

* Develop a LaTex template, to create some pdf output
* Add some more documentation, especially about the header section